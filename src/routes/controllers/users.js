import User from '../../models/User';

export async function get(_req, res) {
  User.find()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.json({ message: err });
    });
}

// get specific post
// router.get('/:postId', (req, res) => {
//   console.log(req.params.postId)
//   Post.findOne({_id: req.params.postId}).then(data => {
//     res.json(data)
//   })
//   .catch(err => {
//     res.json({message: err})
//   })
// })

export async function add(req, res) {
  const user = new User({
    title: req.body.title,
    description: req.body.description,
  });

  user
    .save()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.json({ message: err });
    });
}

// delete post
// router.delete('/:postId', (req, res) => {
//   Post.findOneAndRemove({_id: req.params.postId}).then(data => {
//     res.json(data)
//   })
//   .catch(err => {
//     res.json({message: err})
//   })
// })

// updatePost
// router.patch('/:postId', (req, res) => {
//   Post.updateOne({_id: req.params.postId}, {$set: {title: req.body.title}}).then(data => {
//     res.json(data)
//   })
//   .catch(err => {
//     res.json({message: err})
//   })
// })
