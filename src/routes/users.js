import express from 'express';

import { add, get } from './controllers/users';

export default function getUsersRoutes() {
  const router = express.Router();
  router.get('/', get);
  router.post('/', add);
  return router;
}
